#!/usr/bin/env python
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################

# heavily modified from: oss-fuzz/infra/helper.py

from __future__ import print_function
import argparse
import copy
import datetime
import errno
import glob
import hashlib
import itertools
import json
import multiprocessing
import os
import pipes
import re
import shutil
import subprocess
import sys
import tarfile
import tempfile
import templates
import pprint

# external
import yaml
import jinja2

# local
import storage

OSSFUZZ_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),"oss-fuzz")
BUILD_DIR = os.path.abspath('./build')

BASE_IMAGES = [
    '{baserepo}/base-image',
    '{baserepo}/base-clang',
    '{baserepo}/base-builder',
    '{baserepo}/base-runner',
]

CONFIG = {}

VALID_PROJECT_NAME_REGEX = re.compile(r'^[a-zA-Z0-9_-]+$')
MAX_PROJECT_NAME_LENGTH = 26

AFL_PLOT_IMG = 'registry.gitlab.com/royragsdale/fuzzing-measurement/afl-plot'

if sys.version_info[0] >= 3:
    raw_input = input

def main():
  if not os.path.exists(BUILD_DIR):
    os.mkdir(BUILD_DIR)

  parser = argparse.ArgumentParser('helper.py', description='oss-fuzz helpers')
  parser.add_argument('experiment_config', help='path of experiment yaml')
  subparsers = parser.add_subparsers(dest='command')
  subparsers.required = True

  pull_images_parser = subparsers.add_parser('pull_images',
                                             help='Pull base images.')

  build_experiment_parser = subparsers.add_parser(
      'build_experiment', help='Generate fuzz-ready runners for an experiment.')
  build_experiment_parser.add_argument('--dry', action='store_true', help='generate configs only, no build')
  build_experiment_parser.add_argument('--ci', action='store_true', help='generate a GitLab CI config')
  build_experiment_parser.add_argument('--skip-test', action='store_true', help='Skip testing runners')

  run_experiment_parser = subparsers.add_parser(
      'run_experiment', help='Run an experiment locally')

  process_experiment_parser = subparsers.add_parser(
      'process_experiment', help='Process the results of an experiment')
  process_experiment_parser.add_argument('--html', action='store_true', help='update html only')

  build_project_parser = subparsers.add_parser(
      'build_project', help='Build a project image.')
  build_project_parser.add_argument('project_name')
  build_project_parser.add_argument('--pull', action='store_true',
                                  help='Pull latest base image.')
  build_project_parser.add_argument('--no-pull', action='store_true',
                                  help='Do not pull latest base image.')
  build_project_parser.add_argument('--use-cache', action='store_true',
                                  help='Allow cache')

  build_artifact_parser = subparsers.add_parser(
      'build_artifact', help='Build an artifact image for a given configuration.')
  build_artifact_parser.add_argument('config', help='path of configuration yaml')

  build_runner_parser = subparsers.add_parser(
      'build_runner', help='Build a runner image for a given configuration.')
  build_runner_parser.add_argument('config', help='path of configuration yaml')

  test_runner_parser = subparsers.add_parser(
      'test_runner', help='Test a runner for a given configuration.')
  test_runner_parser.add_argument('config', help='path of configuration yaml')

  execute_runner_parser = subparsers.add_parser(
      'execute_runner', help='Run a runner for a given configuration.')
  execute_runner_parser.add_argument('config', help='path of configuration yaml')

  process_results_parser = subparsers.add_parser(
      'process_results', help='Process the results for a given configuration.')
  process_results_parser.add_argument('config', help='path of configuration yaml')

  push_images_parser = subparsers.add_parser(
      'push_images', help='Push all images for a given configuration.')
  push_images_parser.add_argument('config', help='path of configuration yaml')


  args = parser.parse_args()

  # We have different default values for `sanitizer` depending on the `engine`.
  # Some commands do not have `sanitizer` argument, so `hasattr` is necessary.
  if hasattr(args, 'sanitizer') and not args.sanitizer:
    if args.engine == 'dataflow':
      args.sanitizer = 'dataflow'
    else:
      args.sanitizer = 'address'

  # all commands require an experiment_config
  _load_experiment_config(args.experiment_config)

  if args.command == 'build_project':
    return build_project(args)
  elif args.command == 'pull_images':
    return pull_images(args)
  elif args.command == 'build_experiment':
    return build_experiment(args)
  elif args.command == 'run_experiment':
    return run_experiment(args)
  elif args.command == 'process_experiment':
    return process_experiment(args)
  elif args.command == 'build_artifact':
    return build_artifact(args)
  elif args.command == 'build_runner':
    return build_runner(args)
  elif args.command == 'test_runner':
    return test_runner(args)
  elif args.command == 'execute_runner':
    return execute_runner(args)
  elif args.command == 'process_results':
    return process_results(args)
  elif args.command == 'push_images':
    return push_images(args)

  return 0


def _check_project_exists(project_name):
  """Checks if a project exists."""
  if not os.path.exists(_get_project_dir(project_name)):
    print(project_name, 'does not exist', file=sys.stderr)
    return False

  return True


def _get_command_string(command):
  """Returns a shell escaped command string."""
  return ' '.join(pipes.quote(part) for part in command)


def _get_project_dir(project_name):
  """Returns path to the project."""
  return os.path.join(OSSFUZZ_DIR, 'projects', project_name)


def _get_experiment_dir(experiment_name):
  """Returns path to the experiment directory for the given experiment"""
  return os.path.join(BUILD_DIR, experiment_name)


def _env_to_docker_args(env_list):
  """Turn envirnoment variable list into docker arguments."""
  return sum([['-e', v] for v in env_list], [])


def docker_run(run_args, print_output=True, rm=True):
  """Call `docker run`."""
  command = ['docker', 'run', '-i', '--privileged']
  if rm:
    command.append("--rm")

  command.extend(run_args)

  print('Running:', _get_command_string(command))
  sys.stdout.flush()
  stdout = None
  if not print_output:
    stdout = open(os.devnull, 'w')

  try:
    subprocess.check_call(command, stdout=stdout, stderr=subprocess.STDOUT)
  except subprocess.CalledProcessError as e:
    return False

  return True


def docker_build(build_args, pull=False):
  """Call `docker build`."""
  command = ['docker', 'build']
  if pull:
    command.append('--pull')

  command.extend(build_args)
  print('Running:', _get_command_string(command))
  sys.stdout.flush()

  try:
    subprocess.check_call(command)
  except subprocess.CalledProcessError:
    print('docker build failed.', file=sys.stderr)
    return False

  return True


def docker_pull(image, pull=False):
  """Call `docker pull`."""
  command = ['docker', 'pull', image]
  print('Running:', _get_command_string(command))
  sys.stdout.flush()

  try:
    subprocess.check_call(command)
  except subprocess.CalledProcessError:
    print('docker pull failed.', file=sys.stderr)
    return False

  return True


def docker_push(image):
  """Call `docker push`."""
  command = ['docker', 'push', image]
  print('Running:', _get_command_string(command))
  sys.stdout.flush()

  try:
    subprocess.check_call(command)
  except subprocess.CalledProcessError:
    print('docker push failed.', file=sys.stderr)
    return False

  return True


def docker_cp(container, src, dst):
  command = ['docker', 'cp', '%s:%s' % (container, src), dst]
  print('Running:', _get_command_string(command))
  sys.stdout.flush()

  try:
    subprocess.check_call(command)
  except subprocess.CalledProcessError:
    print('docker cp failed.', file=sys.stderr)
    return False

  return True

def docker_rm(container):
  command = ['docker', 'rm', container]
  print('Running:', _get_command_string(command))
  sys.stdout.flush()

  try:
    subprocess.check_call(command)
  except subprocess.CalledProcessError:
    print('docker rm failed.', file=sys.stderr)
    return False

  return True

def _get_build_id(config):
  """Returns a unique, deterministic, id for a given build configuration"""
  data = [(k, config[k]) for k in sorted(config)]
  # split with '-' to preserve valid DNS name
  return config['target'] + "-" + hashlib.sha256(str(data).encode('UTF-8')).hexdigest()[:8]


def _get_build_dir(experiment_dir, build_id):
  """Returns the directory for a given experiment and build"""
  return os.path.join(experiment_dir, build_id)

def _get_coverage_dir(project_name):
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  coverage_dir = os.path.join(experiment_dir, "coverage", project_name)
  return coverage_dir

def _get_project_img_id(project_name):
  global CONFIG
  return '%s/project/%s' % (CONFIG["project-repo"], project_name)

def _get_artifact_img_id(build_id):
  global CONFIG
  return "%s/artifact/%s" % (CONFIG["artifact-repo"], build_id,)

def _get_runner_img_id(build_id):
  global CONFIG
  return "%s/runner/%s" % (CONFIG["artifact-repo"], build_id,)

def _get_coverage_tag(project_name):
  return "{}-coverage".format(project_name)

def _get_run_name(build_id, run):
  return "%s_run_%s" % (build_id, run)

def _load_yaml(path):
    """Load a yaml configuration"""
    with open(path, 'r') as f:
        data = yaml.safe_load(f)
        return data


def _write_yaml(data, path):
    """Write data to a yaml configuration file"""
    with open(path, 'w') as f:
        yaml.dump(data, f)


def _write_env(config, dst):
  """Writes a fully specified environment to be exported at build and run time"""

  env = ["ARCHITECTURE=x86_64",
         "FUZZING_ENGINE=%s" % config['engine']
         ]

  # TODO(rags): hack to workaround default ASAN
  if 'asan' in config and not config['asan']:
    env += ["SANITIZER=", "SANITIZER_FLAGS=", "SANITIZER_FLAGS_address="]
  elif 'coverage' in config and config['coverage']:
    env += ["SANITIZER=coverage"]
  else:
    env += ["SANITIZER=address"]

  with open(dst, 'w') as out:
    out.write("#!/bin/bash -eu\n")
    for e in env:
      out.write("export %s\n" % e)


def _load_experiment_config(path):
  """All commands require an experimental configuration.  This common loader
  ensures that the global CONFIG is set and base_images are correctly formatted"""
  global CONFIG

  experiment = _load_yaml(path)
  CONFIG = experiment["config"]
  print("Configuration is: ")
  pprint.pprint(CONFIG)


def _build_project(project_name, no_cache=False, pull=False):
  """Build project image."""

  global CONFIG

  if not _check_project_exists(project_name):
    return False

  dockerfile_dir = _get_project_dir(project_name)

  build_args = []
  if no_cache:
    build_args.append('--no-cache')

  # configurable base image
  build_args += ["--build-arg", "BASE_IMAGE=%s/base-builder" % CONFIG["base-repo"]]
  build_args += ['-t', _get_project_img_id(project_name), dockerfile_dir]

  if not docker_build(build_args, pull=pull):
    return False
  return _build_coverage(project_name)


def _build_coverage(project_name):
  """A coverage build is a particular type of artifact. Only need one per project."""
  
  cov_dir = _get_coverage_dir(project_name)
  os.makedirs(cov_dir, exist_ok=True)

  config = {"engine": "libfuzzer", "target": project_name, "coverage": True}

  # setup build dir for Dockerfiles
  _write_yaml(config, os.path.join(cov_dir, "configuration.yml"))
  _write_env(config, os.path.join(cov_dir, "env"))

  # build artifact
  tag = _get_coverage_tag(project_name)
  if not _build_artifact(config, cov_dir, tag):
    return False

  return _build_runner(config, cov_dir, tag)


def _build_artifact(config, build_dir, tag=None):
  b_id = _get_build_id(config) if tag is None else tag
  img_id = _get_artifact_img_id(b_id)

  build_arg = ["--build-arg", "PROJECT_IMAGE=%s" % _get_project_img_id(config['target'])]
  build_arg += ["-f", "images/artifact/Dockerfile", "-t", img_id, build_dir]
  return docker_build(build_arg)


def _build_runner(config, build_dir, tag=None):
  b_id = _get_build_id(config) if tag is None else tag
  base_img = _get_artifact_img_id(b_id)
  runner_tag = _get_runner_img_id(b_id)

  build_arg = ["--build-arg", "ARTIFACT_IMAGE=%s" % base_img]
  build_arg += ["--build-arg", "RUNNER_IMAGE=%s/base-runner" % CONFIG["base-repo"]]
  build_arg += ["-f", "images/runner/Dockerfile", "-t", runner_tag, build_dir]
  return docker_build(build_arg)


def _test_runner(config):
  b_id = _get_build_id(config)
  runner_tag = _get_runner_img_id(b_id)
  return docker_run([runner_tag, "bash", "-c", ". /out/env && test_all"])


def _push_images(config):
  project_name = config['target']
  b_id = _get_build_id(config)
  runner_tag = _get_runner_img_id(b_id)
  artifact_tag = _get_artifact_img_id(b_id)
  project_tag = _get_project_img_id(project_name)
  cov_tag = _get_coverage_tag(project_name)
  coverage_runner = _get_artifact_img_id(cov_tag)
  coverage_artifact = _get_runner_img_id(cov_tag)

  all_tags = [runner_tag, artifact_tag, project_tag, coverage_runner, coverage_artifact]
  res = [docker_push(tag) for tag in all_tags]

  return all(res)


def _fix_base_images(repo):
  global BASE_IMAGES
  new_base = []

  for i in BASE_IMAGES:
    n = i.format(baserepo=repo)
    new_base.append(n)

  BASE_IMAGES = new_base


def _config_to_job_args():
  args = ["-e", "EXP_TIME=%s" % CONFIG["run_time"],
         "-e", "SNAPSHOT_TIME=%s" % CONFIG["snapshot_time"],
         "--cap-add", "SYS_PTRACE"]

  return args

def _check_results_exist(results_dst):
  return os.path.exists(results_dst)

def _run_job(run, build):
  print("RUN: {}".format(run))
  pprint.pprint(build["config"])

  # convenience variables
  b_id = build["build_id"]
  runner_tag = _get_runner_img_id(b_id)
  name = _get_run_name(b_id, run)
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  build_dir = _get_build_dir(experiment_dir, build['build_id'])
  results_dst = os.path.join(build_dir, "%s_results.tar.gz" % run)

  # if results already exist, skip, to allow experiment resumption
  if _check_results_exist(results_dst):
    print("Results already exist, skipping")
    return True

  # ensure no container with this name exists
  docker_rm(name)

  # run the experiment
  args = _config_to_job_args()
  args += ["--name", name, runner_tag]
  if not docker_run(args,rm=False):
    return False

  # collect the results
  if not docker_cp(name, "/tmp/results.tar.gz", results_dst):
    return False

  # clean up the container
  if not docker_rm(name):
    return False

  return True

def _get_results_tars(build_dir):
  result_files = glob.glob(os.path.join(build_dir, "*_results.tar.gz"))
  return result_files

def _compute_coverage(corpus, build, run):
  cov_tag = _get_coverage_tag(build['target'])
  cov_img = _get_runner_img_id(cov_tag)

  name = "compute_coverage"
  # ensure no container with this name exists
  docker_rm(name)

  # TODO(rags): currently everything defaults to just the first target
  target = _get_targets(build)[0]
  command =['-v', '%s:/corpus/%s' % (corpus,target),
    '-e', 'COVERAGE_EXTRA_ARGS=',
    '-e', 'HTTP_PORT=',
    '--name', name,
    cov_img,
    "bash", "-c", ". /out/env && coverage %s && tar -czf /out/report.tar.gz -C /out/report ." % target]

  if not docker_run(command, rm=False):
    return None

  # collect the results
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  b_id = _get_build_id(build)
  build_dst = _get_build_dir(experiment_dir, b_id)
  report_dst = os.path.join(build_dst, "run_{}_report.tar.gz".format(run))
  if not docker_cp(name, "/out/report.tar.gz", report_dst):
    return None

  # clean up the container
  if not docker_rm(name):
    return None


  with tarfile.open(report_dst) as tar:
    tar.extractall(os.path.join(build_dst, "run_{}_report".format(run)))
    with tar.extractfile('./linux/summary.json') as io:
      report = json.load(io)


  # TODO(rags): poc, regions are most granular, % makes sense for comparison within project
  return report['data'][0]['totals']['regions']['percent']


def _get_targets(build):
  b_id = _get_build_id(build)
  runner = _get_runner_img_id(b_id)
  command = ['docker', 'run', '-i', '--rm', runner, 'targets_list']
  targets = subprocess.check_output(command).decode('UTF-8').splitlines()
  return targets


def _afl_plot(out_dir, build, run):
  print("PROCESS: afl-plot")
  name = "afl-plot"
  # ensure no container with this name exists
  docker_rm(name)

  command = ['-v', '%s:/state' % out_dir,
      '--name', name,
      AFL_PLOT_IMG,
      'afl-plot', '/state', '/plot']

  if not docker_run(command, rm=False):
    return False

  # collect the results
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  b_id = _get_build_id(build)
  build_dst = _get_build_dir(experiment_dir, b_id)
  plot_dst = os.path.join(build_dst, "run_{}_plot".format(run))
  if not docker_cp(name, "/plot/.", plot_dst):
    return False

  # clean up the container
  if not docker_rm(name):
    return False

  return True


def _process_tar(tar_file, build, run):
  results = {}
  # extract results tar to a temporary directory
  tmpdir = tempfile.mkdtemp(prefix="fuzz_")
  tar = tarfile.open(tar_file)
  tar.extractall(path=tmpdir)
  print("TAR: extracted to {}".format(tmpdir))

  # collect all snapshots and sort by time
  snapshots = glob.glob(os.path.join(tmpdir, "snapshots/*"))
  snapshots.sort()
  print("TAR: found {} snapshots {}".format(len(snapshots), snapshots))
  final = snapshots[-1]

  # for afl we can extract stats from the fuzzer_stats file
  engine = build['engine']
  if engine == "afl":
    # collect exec stat
    statsfiles = glob.glob(os.path.join(final, "*/fuzzer_stats"))
    if len(statsfiles) != 1:
      print("could not find fuzzer_stats")
      return None
    stats_data = _load_yaml(statsfiles[0])
    results["execs"] = stats_data["execs_done"]

    # collect coverage stat
    corpus = glob.glob(os.path.join(final, "*/queue"))

    plotfile = glob.glob(os.path.join(final, "*/plot_data"))
    if len(plotfile) != 1:
      print("could not find plot_data")
      return None

    out_dir = os.path.dirname(plotfile[0])
    if not _afl_plot(out_dir, build, run):
      return None

  
  # for libfuzzer we extract stats from the end of the output
  elif engine == "libfuzzer":
    with open(os.path.join(tmpdir, "fuzz_log")) as out:
        for line in out:
          if line.startswith("stat::number_of_executed_units:"):
            results["execs"] = int(line.split(" ")[1].strip())

    corpus = glob.glob(os.path.join(final, "*"))

  if len(corpus) != 1:
    print("could not find corpus")
    return None

  cov = _compute_coverage(corpus[0], build, run)
  if cov is None:
    print("no covereage collected")
    return None

  results["coverage"] = cov

  # clean up
  shutil.rmtree(tmpdir)

  return results
      

def _process_results(build, build_dir):
  build_id = _get_build_id(build)

  print("PROCESS: {}".format(build_dir))
  pprint.pprint(build)
  tars = _get_results_tars(build_dir)

  print("PROCESS: found {} results".format(len(tars)))
  
  data = tuple([build_id] + [build[f]  for f in ["target", "engine", "asan"]])
  storage.store_build(data)

  results = {}
  for t in tars:
    run = int(os.path.basename(t).split("_")[0])
    res = _process_tar(t, build, run)

    print(res)
    data = (build_id, run, res["execs"], res["coverage"])
    storage.store_run(data)


def _gen_index(experiment):

  print("PROCESS: generate html index")
  t = jinja2.Template(templates.EXP_INDEX)

  runs = storage.query_stats()
  data = {"experiment":experiment, "runs":runs}
  res = t.render(data)


  dst = os.path.join(_get_experiment_dir(CONFIG["name"]),"index.html")
  with open(dst, 'w') as index:
    index.write(res)


def pull_images(args):
  """Pull base images."""
  _fix_base_images(CONFIG['base-repo'])
  for base_image in BASE_IMAGES:
    if not docker_pull(base_image):
      return 1

  return 0


def build_experiment(args):
  """Build fuzz ready artifacts for an experiment"""

  experiment = _load_yaml(args.experiment_config)
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  print("Starting Experiment: [{}]".format(CONFIG['name']))
  try:
    os.mkdir(experiment_dir)
  except OSError as e:
    if e.errno != errno.EEXIST:
      raise
    if not args.dry and not args.ci:
      print(experiment_dir, 'already exists.', file=sys.stderr)
      return 1

  # generate all possible combinations of options
  options = experiment['options'].keys()
  values = experiment['options'].values()
  matrix = [dict(zip(options,v)) for v in itertools.product(*values)]

  builds = []
  # generate all build configurations
  for target in experiment['targets']:
    for concrete in matrix:
      concrete['target'] = target
      b_id = _get_build_id(concrete)
      build_dst = _get_build_dir(experiment_dir, b_id)
      try:
          os.mkdir(build_dst)
      except OSError as e:
        if e.errno != errno.EEXIST:
          raise
      _write_yaml(concrete, os.path.join(build_dst, "configuration.yml"))
      _write_env(concrete, os.path.join(build_dst, "env"))
      builds.append({"build_id": b_id, "config": copy.copy(concrete)})
  experiment['builds'] = builds
  _write_yaml(experiment, os.path.join(experiment_dir, "experiment.yml"))

  # short circuit after generating configurations if in dry mode
  if args.dry:
    return 0

  if args.ci:
    with open(os.path.join(experiment_dir, ".gitlab-ci.yml"), 'w') as yml:
      template_args = {
          "experiment_name": experiment['config']['name'],
          "experiment_path": args.experiment_config
          }
      yml.write(templates.CI_HEADER.format(**template_args))

      for target in experiment['targets']:
        yml.write(templates.CI_BUILD_PROJECT.format(target=target))

      for build in experiment['builds']:
        b_id = build["build_id"]
        build_path = os.path.relpath(_get_build_dir(experiment_dir, b_id))
        target = build["config"]["target"]
        template_args = {"build_id": b_id, "build_path": build_path, "target": target}
        yml.write(templates.CI_BUILD_ARTIFACT.format(**template_args))

        template_args = {"build_id": b_id, "build_path": build_path}
        yml.write(templates.CI_BUILD_RUNNER.format(**template_args))

        template_args = {"build_id": b_id, "build_path": build_path}
        yml.write(templates.CI_PUSH_RUNNER.format(**template_args))

    return 0

  for project in experiment['targets']:
    # 1. Build project images. This fetches the necessary build dependencies and
    # creates the nessecary coverage artifacts
    if not _build_project(project, no_cache=False, pull=False):
      return 1

  for build in experiment['builds']:
    # 2. Build fuzz artifacts. This runs in the above generated image
    # with the concrete configuration parameters specified to produce
    # an image containing stand-alone fuzz ready artifacts
    config = build['config']
    build_dir = _get_build_dir(experiment_dir, build['build_id'])

    print("BUILD: Building configuration for:")
    pprint.pprint(config)

    if not _build_artifact(config, build_dir):
        print("Error in fuzzer artifact build")
        return 1

    # 3. Create runner image
    print("BUILD: Building runner for:")
    pprint.pprint(config)
    if not _build_runner(config, build_dir):
        print("Error in runner build")
        return 1

    # 4. test runner image
    if args.skip_test:
        continue
    print("BUILD: Testing runner for:")
    pprint.pprint(config)
    if not _test_runner(config):
        print("Error in runner test")
        return 1

def run_experiment(args):
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  experiment = _load_yaml(os.path.join(experiment_dir,"experiment.yml"))
  print("Running Experiment: [{}]".format(CONFIG['name']))

  num_builds = len(experiment["builds"])
  total_runs = CONFIG["runs"] * num_builds
  total_time = (total_runs * CONFIG["run_time"])
  print("Executing {} x {}s runs each for {} builds".format(CONFIG["runs"], CONFIG["run_time"], num_builds))
  print("Total Time: {:0>8}".format(str(datetime.timedelta(seconds=total_time))))

  jobs = itertools.product(range(CONFIG["runs"]), experiment["builds"])
  for j in jobs:
    if not _run_job(*j):
      return 1
  return 0


def process_experiment(args):
  experiment_dir = _get_experiment_dir(CONFIG['name'])
  experiment = _load_yaml(os.path.join(experiment_dir,"experiment.yml"))
  print("Processing Experiment: [{}]".format(CONFIG['name']))

  # get a db connection
  storage.connect(os.path.join(experiment_dir, "results.db"))

  _gen_index(experiment)
  if args.html:
    return 0

  for build in experiment["builds"]:
    build_id = build["build_id"]
    config = build["config"]
    build_dir = _get_build_dir(experiment_dir, build_id)
    _process_results(config, build_dir)


def build_project(args):
  """Build docker image for project."""
  if args.pull and args.no_pull:
    print('Incompatible arguments --pull and --no-pull.')
    return 1

  if args.pull:
    pull = True
  elif args.no_pull:
    pull = False
  else:
    y_or_n = raw_input('Pull latest base images (compiler/runtime)? (y/N): ')
    pull = y_or_n.lower() == 'y'

  if pull:
    print('Pulling latest base images...')
  else:
    print('Using cached base images...')

  # If build_project is called explicitly, don't use cache.
  no_cache = True
  if args.use_cache:
      no_cache = False
  if _build_project(args.project_name, no_cache=no_cache, pull=pull):
    return 0

  return 1


def build_artifact(args):
  """Given a specific configuration file, build an image with fuzz ready artifacts"""
  config = _load_yaml(args.config)
  build_dir = os.path.dirname(args.config)
  if  _build_artifact(config, build_dir):
    return 0
  return 1


def build_runner(args):
  config = _load_yaml(args.config)
  build_dir = os.path.dirname(args.config)
  if _build_runner(config, build_dir):
    return 0
  return 1


def test_runner(args):
  config = _load_yaml(args.config)
  if _test_runner(config):
    return 0
  return 1


def execute_runner(args):
  config = _load_yaml(args.config)
  build_dir = os.path.dirname(args.config)
  build_id = _get_build_id(config)
  build = {"build_id": build_id, "config": config}

  res = _get_results_tars(build_dir)
  run = len(res) # 0 indexed

  if _run_job(run, build):
    return 0
  return 1


def process_results(args):
  config = _load_yaml(args.config)
  build_dir = os.path.dirname(args.config)

  experiment_dir = _get_experiment_dir(CONFIG['name'])
  storage.connect(os.path.join(experiment_dir, "results.db"))
  if _process_results(config, build_dir):
    return 0
  return 1

def push_images(args):
  config = _load_yaml(args.config)
  if _push_images(config):
    return 0
  return 1


if __name__ == '__main__':
  sys.exit(main())
